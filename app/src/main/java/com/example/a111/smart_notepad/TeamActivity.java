package com.example.a111.smart_notepad;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


/**
 * Created by 111 on 18.12.2017.
 */

public class TeamActivity extends MainActivity {
    final Context context = this;
    public ImageButton R_note ;
    public Button button_note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_team);

        R_note = (ImageButton) findViewById(R.id.mImageButton_R_note);
        button_note = (Button) findViewById(R.id.button__note);

        R_note.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                View redView =li.inflate(R.layout.red,null);
                AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);
                mDialogBuilder.setView(redView);
                final EditText userInput = (EditText) redView.findViewById(R.id.input_text);

                mDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog,int id) {
                                        button_note.setText(userInput.getText());
                                    }
                                })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = mDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.button_activity_composition):
                Intent int_profile_act = new Intent(TeamActivity.this, CompositionActivity.class);
                startActivity(int_profile_act);
                break;
            case (R.id.mImageButton_D_name_team):
                button_note = (Button) findViewById(R.id.button__note);
                button_note.setText("");
                break;
        }
    }
}
