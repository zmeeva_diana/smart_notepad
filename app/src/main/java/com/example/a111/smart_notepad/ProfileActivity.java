package com.example.a111.smart_notepad;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
/**
 * Created by 111 on 02.12.2017.
 */

public class ProfileActivity extends MainActivity {
    private ImageButton mImageButtonName;
    private ImageButton mImageButtonPosition;
    private ImageButton mImageButtonHeight;
    private ImageButton mImageButtonWeight;
    private ImageButton mImageButton_ph_number;
    private ImageButton mImageButtonFeatures;
    private ImageButton  mImageButtonHome;

    private TextView Exe_text;

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile_player);
        mImageButtonName = (ImageButton) findViewById(R.id.mImageButtonName);
        mImageButtonPosition = (ImageButton) findViewById(R.id.mImageButtonPosition);
        mImageButtonHeight = (ImageButton) findViewById(R.id.mImageButtonHeight);
        mImageButtonWeight = (ImageButton) findViewById(R.id.mImageButtonWeight);
        mImageButton_ph_number = (ImageButton) findViewById(R.id.mImageButton_ph_number);
        mImageButtonFeatures = (ImageButton) findViewById(R.id.mImageButtonFeatures);
        mImageButtonHome = (ImageButton) findViewById(R.id.mImageButtonHome);

        Exe_text  = (TextView) findViewById(R.id.Exe_text);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.mImageButtonHome):
                Intent int_main_act= new Intent(ProfileActivity.this, MainActivity.class);
                startActivity(int_main_act);
                break;
        }
    }

}

