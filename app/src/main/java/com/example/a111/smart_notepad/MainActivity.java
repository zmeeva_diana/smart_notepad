package com.example.a111.smart_notepad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


public class MainActivity extends AppCompatActivity {
    final Context context = this;
    public ImageButton R_name_team ;
    public Button name_team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        R_name_team = (ImageButton) findViewById(R.id.mImageButton_R_name_team);
        name_team = (Button) findViewById(R.id.button_name_team);

        R_name_team.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = LayoutInflater.from(context);
                View redView =li.inflate(R.layout.red,null);
                AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(context);
                mDialogBuilder.setView(redView);
                final EditText userInput = (EditText) redView.findViewById(R.id.input_text);

                mDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener(){
                                    public void onClick(DialogInterface dialog,int id) {
                                        name_team.setText(userInput.getText());
                                    }
                                })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = mDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.button_name_team):
                Intent int_main_act= new Intent(MainActivity.this, TeamActivity.class);
                startActivity(int_main_act);
                break;
            case (R.id.mImageButton_D_name_team):
                name_team = (Button) findViewById(R.id.button_name_team);
                name_team.setText("");
                break;
        }
    }

}



