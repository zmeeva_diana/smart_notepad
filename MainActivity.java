package com.example.a111.smart_notepad;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ImageButton Ib1;
    ImageButton Ib2;
    ImageButton Ib3;
    ImageButton Ib4;
    ImageButton Ib5;
    ImageButton Ib6;
    ImageButton Home;
    TextView TW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Ib1 = (ImageButton) findViewById(R.id.Ib1);
        Ib2 = (ImageButton) findViewById(R.id.Ib2);
        Ib3 = (ImageButton) findViewById(R.id.Ib3);
        Ib4 = (ImageButton) findViewById(R.id.Ib4);
        Ib5 = (ImageButton) findViewById(R.id.Ib5);
        Ib6 = (ImageButton) findViewById(R.id.Ib6);
        Home = (ImageButton) findViewById(R.id.Home);

        TW = (TextView) findViewById(R.id.TW);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.Home):
                TW.setText("Example home");
                break;
        }
    }

}



